# Estagio 1 - Será responsavel em construir nossa aplicação
FROM node:10.13.0-slim as angular
WORKDIR /app
COPY package.json /app/
RUN npm i npm@latest -g
RUN npm install --silent
COPY ./ /app/
ARG env=prod
RUN npm run build

# Estagio 2 - Será responsavel por expor a aplicação
FROM nginx:alpine
VOLUME /var/cache/nginx
COPY --from=angular /app/dist/BookStoreOnline /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

# TESTAR IMAGEM
# docker build -t bookStoreOnline .

#EXECUTAR IMAGEM
# docker run -p 8081:80 bookStoreOnline
