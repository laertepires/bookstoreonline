import { Component, OnInit, Input, ViewEncapsulation, forwardRef } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    }
  ]
})
export class InputComponent implements OnInit {

  @Input() control: FormControl = new FormControl();
  @Input() type: string;
  @Input() placeholder: string;
  @Input() name: string;
  @Input() class: string;
  hide = true;


  constructor() { }

  ngOnInit(): void {
    console.log('formControl -> ', this.control );
  }


}
