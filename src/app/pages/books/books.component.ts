import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BooksService } from 'src/app/core/services/books.services';
import {  BookType } from 'src/app/core/types/book.types';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BooksComponent implements OnInit {

  /**
   * array books
   */
  books: BookType[] = [];

  /**
   * form
   */
  form: FormGroup;

  /**
   * slider config
   */

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  slideConfig = {
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    infinite: false,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  };

  constructor(
    public service: BooksService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar
  ) { }

  /**
   * init from component
   */
  ngOnInit(): void {

    this.form = this.formBuilder.group({
      search: ['']
    });

    this.service.getAllBooks().subscribe((data: BookType[]) => {
      this.books = data;
    });
  }

  /**
   * submit search
   */
  onSubmit() {
    this.service.searchBookByTitle(this.form.get('search').value).subscribe((data: BookType[]) => {
      this.books = data;
    });
  }

  rent(book: BookType) {
    const payload: any = book;
    payload.available = true;

    this.service.updateBook(book.id, payload).subscribe(data => {
      this.snackBar.open('Livro alugado com Sucesso', 'Undo', {
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    });
  }
}
