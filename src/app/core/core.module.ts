import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BooksService } from './services/books.services';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    BooksService,
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() core: CoreModule ){
    if (core) {
      throw new Error('You should import core module only in the root module');
    }
  }
 }
