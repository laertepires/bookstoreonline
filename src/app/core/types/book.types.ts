export class BookType {
  id: number;
  title: string;
  author: string;
  image: string;
  description: string;
  available: boolean;
  availableUser: number;
}
