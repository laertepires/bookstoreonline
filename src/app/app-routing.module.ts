import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { PagesComponent } from './pages/pages.component';
import { AuthGuard } from './core/guards/auth.guard';
import { BooksComponent } from './pages/books/books.component';
import { BookDetailsComponent } from './pages/book-details/book-details.component';
import { BookDetailsResolver } from './pages/book-details/book.resolver';
import { RegisterComponent } from './pages/register/register.component';


const routes: Route[] = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: AuthComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'list',
    component: PagesComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'books',
        pathMatch: 'full'
      },
      {
        path: 'books',
        component: BooksComponent
      },
      {
        path: 'books/:id',
        component: BookDetailsComponent,
        resolve: { book: BookDetailsResolver },
      },
    ]
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [
    RouterModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: [BookDetailsResolver]
})
export class AppRoutingModule { }
